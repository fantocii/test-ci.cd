[![pipeline status](https://gitlab.com/fantocii/test-ci.cd/badges/main/pipeline.svg)](https://gitlab.com/fantocii/test-ci.cd/-/commits/main)
# Тестовый модуль
Тренировочный модуль, который будет покрываться тестами и на его основе будут создаваться нормальные модули

## Задачи:
- [x] Написать простой модуль
- [x] Покрыть его юнит тестами
- [ ] Настроить CI/CD
    - [ ] Тест кода
    - [ ] Пуш кода на 160 сервер
    - [ ] Создание отчета
    - [ ] Отчет на почту

## Процесс
Путь для пользовательских модулей - `/home/username/.local/lib/python3.6/site-packages/`
